document.addEventListener('keydown', e => {

    const buttons = document.getElementsByClassName('btn')

    for (let i = 0; i < buttons.length; i++){
        if (buttons[i].textContent === e.key.toUpperCase() || buttons[i].textContent === e.key){
            const delBlu = document.getElementsByClassName('btn-blue')
            if (delBlu.length > 0){
                for (let n = 0; n < delBlu.length; n++){
                    delBlu[n].classList.remove('btn-blue')
                }
            }

            buttons[i].classList.add('btn-blue')
        }
    }
})
